<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

/**
 * List of enabled modules for this application.
 *
 * This should be an array of module namespaces used in the application.
 */
return [
    'Zend\Mvc\Plugin\FlashMessenger',
    'Zend\Navigation',
    'Zend\Mvc\I18n',
    'Zend\Mail',
    'Zend\Log',
    'Zend\Session',
    'Zend\I18n',
    'Zend\InputFilter',
    'Zend\Hydrator',
    'Zend\Filter',
    'Zend\Paginator',
    'Zend\Db',
    'Zend\Form',
    'Zend\Router',
    'Zend\Validator',
    'ZF\Versioning',
    'ZF\ApiProblem',
    'ZF\ContentNegotiation',
    'ZF\Rpc',
    'ZF\MvcAuth',
    'ZF\Hal',
    'ZF\Rest',
    'ZF\ContentValidation',
    'ZF\Apigility',
    'ZF\Configuration',
    'ZF\\OAuth2',
    'ZF\\MvcAuth',
    'Application',
    'MxmDateTime',
    'MxmBlog',
    'Soflomo\Purifier',
    'MxmUser',
    'MxmRbac',
    'MxmMail',
    'MxmApi',
    'MxmAdmin',
    'MxmFile',
    'MxmGame',
];
